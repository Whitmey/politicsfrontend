import React from 'react';
import {
  BrowserRouter as Router,
  Switch,
  Route
} from "react-router-dom";
import CssBaseline from '@material-ui/core/CssBaseline';
import './App.css';
import Navigation from './Components/Navigation';
import DashboardView from './Components/DashboardView';
import GovernmentView from './Components/GovernmentView';

export default function App() {
  return (
    <Router>

      <div className="appRoot">
        <CssBaseline />

        <Navigation />

        <Switch>
          <Route exact path="/">
            <DashboardView />
          </Route>
          <Route path="/government">
            <GovernmentView />
          </Route>
          <Route path="/dashboard">
            dashboard
          </Route>
        </Switch>
      </div>

    </Router>
  );
}
